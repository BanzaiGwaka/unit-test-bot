package fr.umlv.unitestbot;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;

import static net.dv8tion.jda.api.entities.Message.Attachment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class MyEventListener extends ListenerAdapter {
    private final String pathToResources    = "../unit-test-bot/src/main/resources/";
    private final String pathToCode         = pathToResources + "code/";
    private final String prefix             = "!";
    private final Database database;
    private final CommandHandler commandHandler;

    public MyEventListener(@Nonnull Database database, @Nonnull CommandHandler commandHandler) {
        this.commandHandler = commandHandler;
        this.database = database;
    }

    /**
     *  Method called whenever a message is sent in a server with the bot
     *  @param event {GuildMessageReceivedEvent}
     */
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        if(event.getAuthor().isBot() || !event.getMessage().getContentRaw().startsWith(this.prefix)) return;
        /*System.out.println("MESSAGE RECEIVED" +
                           "\nAuthor : " + event.getAuthor().getName() +
                           "\nMember : " + event.getMember().getRoles() +
                           "\n " + event.getMessage().getAttachments().size());
         */


        String receivedMessage  = event.getMessage().getContentRaw();
        if(receivedMessage.startsWith(this.prefix + "help")) {
            displayHelpMessage(event);
            return;
        }
        switch (receivedMessage.charAt(1)) {
            case 'c' -> loadTest(event);
            case 'j' -> loadCode(event);
            case 'e' -> System.out.println("maintenance");

            default -> System.out.println("Type !help for help with commands");
        }
        event.getChannel().sendMessage("Message received : " + receivedMessage ).queue();
    }

    /**
     * Used to load a java code and begin test with it if a test with the correspondant name already exists
     * @param event {GuildMessageReceivedEvent} Describe a discord event
     */
    private void loadCode(GuildMessageReceivedEvent event) {
        createFolder(this.pathToCode);
        for(Attachment attachment : event.getMessage().getAttachments()) {
            if(isCode(attachment.getFileName())) {
                String nameOfCode = attachment.getFileName();
                String nameOfTest = nameOfCode.replace(".java", "Test.class");

                System.out.println(nameOfCode);
                System.out.println(nameOfTest);

                downloadFile(attachment, this.pathToCode + nameOfCode);

                if(database.checkTest(nameOfTest)) {
                    System.out.println("Condition of test yes");
                    this.commandHandler.execTest(attachment.getFileName(), this.pathToCode, database.getTest(nameOfTest));
                    // database.updateTest(nameOfTest); count++
                }
                deleteFile(this.pathToCode + nameOfCode);
            }
        }
    }

    /**
     * Used to download the byte content of a test and to load it into database
     * @param event {GuildMessageReceivedEvent} Describe a discord event
     */
    private void loadTest(GuildMessageReceivedEvent event) {
        for(Attachment attachment : event.getMessage().getAttachments()) {
            if(isTest(attachment.getFileName())) {
                addDatabaseTest(attachment, event.getAuthor().getName());
            }
        }
    }

    /**
     * Delete the file in argument, must delete java file used to test
     * @param pathOfFile {String} Path of the file to delete
     */
    private void deleteFile(String pathOfFile) {
        System.out.println(pathOfFile);
        File file = new File(pathOfFile);
        if(file.delete()) {
            System.out.println("file deleted : " + pathOfFile);
        } else {
            System.out.println("file NOT deleted : " + pathOfFile);
        }
    }

    /**
     * !! Deprecated
     * Add or update an existent test in database
     * @param attachment {Attachment} Used to get the author and the name of the test
     * @param pathOfTestFile {String} Describe the path of the test and is used to get byte array from it
     * @param author {String} Describe the current author of the test
     */
    private void addDatabaseTestV2(Attachment attachment, String pathOfTestFile, String author) {
        try {
            byte[] bytesOfTest = Files.readAllBytes(Path.of((pathOfTestFile)));
            //System.out.println("FIRST METHOD "  + bytesOfTest);
            Files.write(Path.of(this.pathToResources + "/target.class"), bytesOfTest);
            // database function with bytes code
            database.addTest(attachment.getFileName(), author, null);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    /**
     * Add or update an existent test in database, add the content of the test as byte array
     * @param attachment {Attachment} Used to get the content name of the test
     * @param author {String} Describe the current author of the test
     */
    private void addDatabaseTest(Attachment attachment, String author) {
        attachment.retrieveInputStream().thenAccept(in -> {
            try {
                byte[] bytes = in.readAllBytes();
                if(database.checkTest(attachment.getFileName())) database.updateTest(attachment.getFileName(), bytes);
                else database.addTest(attachment.getFileName(), author, bytes);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }).exceptionally(t -> { // handle failure
            t.printStackTrace();
            return null;
        });
    }

    /**
     * Create a new folder if not exist
     * @param pathInString {String} Describe the path to the folder to create
     */
    private void createFolder (String pathInString) {
        Path path = Paths.get(pathInString);
        if (Files.notExists(path)) {
            try {
                Files.createDirectories(path);
            }
            catch (IOException e) {
                throw new AssertionError(e);
            }
        }
    }

    /**
     * Used to download an attachemnt into the pathOfFile folder
     * @param attachment {Attachement} Used to get the content of an attachment
     * @param pathOfFile {String} Describe the path to download the file
     */
    private void downloadFile(Attachment attachment, String pathOfFile) {
        //System.out.println(pathOfFile);
        attachment.downloadToFile(pathOfFile)
                .thenAccept(file -> System.out.println("Saved attachment test file to " + file.getName()))
                .exceptionally(t ->
                {
                    t.printStackTrace();
                    return null;
                });
    }

    /**
     *  Checks if the selected filename is a valid Test name
     *  @param filename name of the file to test
     *  @return true if the Test name is valid, else false
     */
    public boolean isTest(String filename) {
        return filename.endsWith("Test.class");
    }

    /**
     *  Checks if the selected filename is a valid Code name
     *  @param filename name of the file to test
     *  @return true if the Code name is valid, else false
     */
    public boolean isCode(String filename) {
        return filename.endsWith(".java");
    }

    /**
     *  Method called when the message is !help which displays the commands
     *  @param event {GuildMessageReceivedEvent}
     */
    private void displayHelpMessage(GuildMessageReceivedEvent event){
        StringBuilder sb = new StringBuilder();
        sb.append("Unit Test Bot developped by Cedric Clerfayt and Leo Beauzee\n\n");
        sb.append("COMMANDS :\n\n");
        sb.append("!c FooTest.class : for teachers only, proposes a new test for the class Foo\n");
        sb.append("!j Foo.java : for students and teachers, proposes a new class Foo to test\n");
        event.getChannel().sendMessage(sb).queue();
    }

}
