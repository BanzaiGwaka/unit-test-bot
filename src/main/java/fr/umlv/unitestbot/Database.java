package fr.umlv.unitestbot;

import org.jdbi.v3.core.Jdbi;

import java.sql.*;

public class Database {
    private final String jdbiConnect = "jdbc:sqlite:src/main/resources/unitTestBot.db";
    final private Jdbi jdbi;


    Database() {
        final String userName = "unitBot";
        final String password = "unitBotPassword";
        this.jdbi = Jdbi.create(this.jdbiConnect, userName, password);
    }

    /**
     * Initializes the Database with the tables test, save_result and save_result_user
     */
    public void init() {
        this.jdbi.useHandle(handle -> {
            handle.execute("CREATE TABLE IF NOT EXISTS test (name VARCHAR PRIMARY KEY, " +
                    "creator VARCHAR, " +
                    "dateCreation datetime default CURRENT_TIMESTAMP, " +
                    "description VARCHAR, " +
                    "aliveUntil datetime, " + //default DATEADD(DD,7,GETDATE())
                    "used INT default 0)");
            handle.execute("CREATE TABLE IF NOT EXISTS save_result (id INT AUTO_INCREMENT, " +
                    "nameTest VARCHAR, " +
                    "user VARCHAR, " +
                    "result INT, " +
                    "dateOfResult DATE, " +
                    "questions INT, " +
                    "bestScore BOOLEAN, " +
                    "PRIMARY KEY (id)," +
                    "FOREIGN KEY (nameTest ) REFERENCES test(name))");

            handle.execute("CREATE TABLE IF NOT EXISTS save_result_user (id INT AUTO_INCREMENT, " +
                    "nameOfTest VARCHAR, " +
                    "idOfResult INT, " +
                    "idOfQuestion INT, " +
                    "user VARCHAR, " +
                    "description VARCHAR, " +
                    "result BOOLEAN, " +
                    "PRIMARY KEY (id)," +
                    "FOREIGN KEY (nameOfTest) REFERENCES test(name), " +
                    "FOREIGN KEY (idOfResult) REFERENCES save_result(id))");
        });
    }

    /**
     * Creates a new row of a test in the table test
     * @param fileName {String} the name of the added attachment
     * @param author {String} the name of the file's author
     * @param content {byte[]} the content of the file
     */
    public void addTest(String fileName, String author, byte[] content) {
        String request = "INSERT INTO test (name, creator, description, aliveUntil)" +
                "VALUES ( '"+ fileName +  "', '" + author + "', '" + content + "', CURRENT_TIMESTAMP)";
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            st.executeUpdate(request);
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Update the content of a test that already exists
     * @param fileName {String} the name of the added attachment
     * @param content {byte[]} the new content of the file
     */
    public void updateTest(String fileName, byte[] content) {
        String request = "UPDATE test SET description = '" + content + "' WHERE name = '" + fileName + "'";
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            st.executeUpdate(request);
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Checks if a test already exists in database
     * @param nameOfTest {String} the name of the test that must be checked
     * @return true if the test already exists, false else
     */
    public boolean checkTest(String nameOfTest) {
        String requestToSearchFile = "SELECT * from test WHERE name = '" + nameOfTest + "'";
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(requestToSearchFile);
            if(!rs.next()) {
                conn.close();
                return false;   //create new row
            }
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;    //update the corresponding row
    }

    /**
     * Method which returns the content of a test
     * @param nameOfTest {String} the name of the test to get
     * @return the byte array containing the content of the file
     */
    public byte[] getTest(String nameOfTest) {
        String request = "SELECT description from test WHERE name = '" + nameOfTest + "'";
        byte[] content = new byte[1024];
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(request);
            content = rs.getBytes(1);
            //System.out.println(content);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return content;
    }

    /**
     * Used to add the row of a result to the table save_result
     * @param nameOfTest {String} the name of the test
     * @param user {String} the name of the user
     * @param result {int} the result
     * @param numberOfQuestions {int} the number of questions
     */
    public void addResult(String nameOfTest, String user, int result, int numberOfQuestions) {
        String request = "INSERT INTO save_result(nameTest, user, result, dateOfResult, questions, bestScore " +
                         "VALUES ('" + nameOfTest + "', '" + user + "', " + result + ", CURRENT_TIMESTAMP, " + numberOfQuestions + ", true)";
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            st.executeUpdate(request);
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Method which returns the content of a result
     * @param nameOfTest {String} the name of the test from which we want the result
     * @return the result obtained
     */
    public int getResult(String nameOfTest) {
        String request = "SELECT result FROM save_result WHERE nameTest = '" + nameOfTest + "'";
        int result = 0;
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(request);
            result = rs.getInt(1);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Updates the result of an already existing result
     * @param nameOfTest {String} the name of the test result to update
     * @param newresult {int} the new result
     */
    public void updateResult(String nameOfTest, int newresult) {
        String request = "UPDATE save_result SET result = '" + newresult + "' WHERE name = '" + nameOfTest + "'";
        try {
            Connection conn = DriverManager.getConnection(jdbiConnect);
            Statement st = conn.createStatement();
            st.executeUpdate(request);
            conn.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
