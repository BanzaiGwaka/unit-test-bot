package fr.umlv.unitestbot;


import org.junit.jupiter.engine.JupiterTestEngine;
import org.junit.platform.engine.*;
import org.junit.platform.launcher.LauncherDiscoveryRequest;
import org.junit.platform.launcher.core.LauncherDiscoveryRequestBuilder;

import java.lang.reflect.UndeclaredThrowableException;
import java.util.Optional;

import static org.junit.platform.engine.discovery.DiscoverySelectors.selectClass;
import static org.junit.platform.engine.discovery.DiscoverySelectors.selectPackage;

public class CommandHandler {
    private final Database database;

    public CommandHandler(Database database) {
        this.database = database;
    }

    /***
     * Used to execute a .class test on a java file given by a student.
     * Use a private TestClassLoader to create a class from a name and a byte array
     * Use a Launcher Discovery Request Class to create request to use with the TestDescriptoer class
     * Creation of private classes to add no parameters and to configure the output of a test
     * @param nameOfTestClass Name of the .class file test, is used to created it as a class with the byte array
     * @param pathToJava    Path to the java file to test
     * @param testContent   Array of byte represent the content of a class, stored in database
     */
    public void execTest(String nameOfTestClass, String pathToJava, byte[] testContent) {
        try {
            System.out.println("Exec Test");
            Class<?> testClass = new TestClassLoader().createClass(nameOfTestClass, testContent, 0, testContent.length);
            System.out.println(testClass);
            LauncherDiscoveryRequest request = LauncherDiscoveryRequestBuilder
                    .request()
                    .selectors(
                            selectPackage(pathToJava),
                            selectClass(testClass)
                    ).build();
            JupiterTestEngine engine = new JupiterTestEngine();

            UniqueId uniqueId   = UniqueId.forEngine(engine.getId());
            TestDescriptor testDescriptor = engine.discover(request, uniqueId);

            NoOpEngineExecutionListener listener = new NoOpEngineExecutionListener();
            NoConfigurationParameters parameters = new NoConfigurationParameters();

            engine.execute(new ExecutionRequest(testDescriptor, listener, parameters));
        } catch (UndeclaredThrowableException e) {
            throw e;
        }

    }

    /**
     * Used to create a class from a byte array
     */
    private static class TestClassLoader extends ClassLoader {
        Class<?> createClass(String name, byte[] b, int off, int len) {
            return super.defineClass(name, b, off, len);
        }
    }

    /**
     * Class used to describe current Test
     * Override multiple method to access execution test
     */
    private static class NoOpEngineExecutionListener implements EngineExecutionListener {
        @Override
        public void executionSkipped(TestDescriptor testDescriptor, String reason) {
            System.out.println(testDescriptor.getDisplayName() + " REASON " + reason);
        }

        @Override
        public void executionStarted(TestDescriptor testDescriptor) {
            System.out.println(testDescriptor.getDisplayName() + " Test DESCIPRRT " +  testDescriptor);
        }

        @Override
        public void executionFinished(TestDescriptor testDescriptor, TestExecutionResult testExecutionResult) {
            System.out.println(testDescriptor.getDisplayName() + " Test RESULT " + testExecutionResult.getStatus());
            // Database
            // database.addUniqueResult(nameOfTest, user, idResult, );
        }
    }

    /**
     * Private class use to configure execution Test
     * No parameters to add
     */
    private static class NoConfigurationParameters implements ConfigurationParameters{
        @Override
        public Optional<String> get(String s) {
            return Optional.empty();
        }

        @Override
        public Optional<Boolean> getBoolean(String s) {
            return Optional.empty();
        }

        @Override
        public int size() {
            return 0;
        }
    }
}
