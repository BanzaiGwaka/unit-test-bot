package fr.umlv.unitestbot;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import javax.security.auth.login.LoginException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Bot {
    private final String token = "Nzk2MTAyMTQ2MzA2MjExODYw.X_TB5g.Vbe4J_2y91C9YbI4_lJash74Y5Y";

    /**
     * Main function used to create : the Bot object
     *                                the Database object and initialize it
     *                                the CommandHandler object
     *                                the MyEventListener object
     * Creates the JDA, sets its token and other parameters and build it
     * @param args
     */
    public static void main(String[] args) {
        Bot bot                         = new Bot();
        Database database               = new Database();
        database.init();    //

        CommandHandler commandHandler   = new CommandHandler(database);
        MyEventListener myEventListener = new MyEventListener(database, commandHandler);

        try {
            JDA jda = JDABuilder.createDefault(bot.token)
                    .disableCache(CacheFlag.MEMBER_OVERRIDES, CacheFlag.VOICE_STATE)
                    .setActivity(Activity.playing("Bot works"))
                    .addEventListeners(myEventListener)
                    .build();

            jda.awaitReady();
        } catch (LoginException e) {
            System.out.println("Wrong login");
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
